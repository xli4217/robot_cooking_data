import numpy as np

from plotly.graph_objs import Scatter, Scatter3d, Figure, Layout
import plotly.graph_objs as go
import plotly.figure_factory as ff

import glob
import pandas as pd
import os

class Viz(object):

    '''
    A utility class for visualization of logged data
    '''

    def __init__(self):
        pass

    def plotly_plot_traj(self, data, x_label, y_label, dim, z_label=None, legends=None, title=None, exclude_value=None):
        '''
        data = [{'x':x, 'y':y}, ...] or [{'x':x, 'y':y, 'z':z}, ...] depending on dimension
        '''
        traces = []
        for i, item in enumerate(data):
            if dim == 2:
                if legends:
                    trace = go.Scatter(x=item['x'],
                                       y=item['y'],
                                       name=legends[i],
                                       line=dict(
                                           color='#1f77b4',
                                           width=1))
                else:
                    trace = go.Scatter(x=item['x'],
                                       y=item['y'],
                                       showlegend=False,
                                       line=dict(
                                           color='#1f77b4',
                                           width=1))
              
            elif dim == 3:
                if legends:
                    trace = go.Scatter3d(x=item['x'],
                                         y=item['y'],
                                         z=item['z'],
                                         name=legends[i],
                                         mode="lines",
                                         line=dict(
                                             color='#1f77b4',
                                             width=1))
                else:
                    trace = go.Scatter3d(x=item['x'],
                                         y=item['y'],
                                         z=item['z'],
                                         showlegend=False,
                                         mode="lines",
                                         line=dict(
                                             color='#1f77b4',
                                             width=1))
       
            traces.append(trace)

        if dim == 2:
            layout = go.Layout(
                title=title,
                xaxis=dict(title=x_label),
                yaxis=dict(title=y_label)
            )
        elif dim == 3:
            layout = go.Layout(
                title=title,
                xaxis=dict(title=x_label),
                yaxis=dict(title=y_label),
                #zaxis=dict(title=z_label)
            )
       

        fig = go.Figure(data=traces, layout=layout)

        return layout, fig

        
    def plotly_plot(self, data_path, x_label, y_label, legend=None, title=None, spread_interval=None):
        '''
        returns a Figure object to be called by the plotly iplot() function
        
        '''
        df = pd.read_csv(data_path)
        if not title:
            title = y_label

        x = df[x_label].as_matrix()
        y = df[y_label].as_matrix()

        if spread_interval:
            idx = np.where(x % spread_interval == 0)
            x = x[idx]
            y = y[idx]
            
        if isinstance(y[0], str):
            not_null_idx = np.where(y != 'None')
            row_idx = np.unique(not_null_idx[0])
            x = x[row_idx]
            y = y[row_idx].astype(np.float64)
            
        data = [
            go.Scatter(
                x=x,
                y=y,
                name=legend
            )
        ]

            
        layout = go.Layout(
            title=title,
            xaxis=dict(title=x_label),
            yaxis=dict(title=y_label)
        )

        fig = go.Figure(data=data, layout=layout)

        return data, layout, fig

    def plotly_plot_dir(self, data_dir, x_label, y_label, legend=None, title=None, spread_interval=None, average=False):

        if os.path.isdir(data_dir):
            csv_path_list = []
            hyperparam_string_list = []
            M = np.array([])
            for root, dirs, files in os.walk(data_dir):
                if dirs:
                    hyperparam_string_list = dirs
                for name in files:
                    if "csv" in name and "~" not in name:
                        csv_path_list.append(os.path.join(root, name))

            data_list = []
            for csv_path, hyperparam_string in zip(csv_path_list, hyperparam_string_list):
                data, layout, _ = self.plotly_plot(csv_path, x_label, y_label, legend=hyperparam_string, title=title, spread_interval=spread_interval)
                data_list.append(data[0])

            data_list_sorted = []
            for data in data_list:
                x = data['x']
                y = data['y']

                sorted_idx = np.argsort(x)
                data['x'] = x[sorted_idx]
                data['y'] = y[sorted_idx]
                data_list_sorted.append(data)
            data_list = data_list_sorted
                
            if average:
                x = data_list[0]['x']
                y = []
                for d in data_list:
                    y.append(d['y'])
                y = np.array(y)
                y = np.mean(y, axis=0)

                data_list = [
                    go.Scatter(
                        x=x,
                        y=y,
                        name=legend
                    )
                ]
 
            fig = go.Figure(data=data_list, layout=layout) 
                
            return data_list, layout, fig

        else:
            raise ValueError("invalid data directory")

            
    def plotly_plot_distribution(self, x_label, y_label, std_scale=2, data=[], dist_path_list=[], legends=None, title=None, spread_interval=None):
        '''
        dist_path_list: a list containing directories holding data for each distribution, for example ["dir1", "dir2", ...] where "dir1" has n csv files with data for the first distribution, and so on

        data: is a list of np arrays, each of data[i] is a 2D array representing a distribution to be plot (row is timestep, col is sample traj)
        '''
        if not len(data) != 0 and not any(dist_path_list):
            raise ValueError("need one of data and dist_path_list")
            
        if any(dist_path_list):
            data = []
            data_x = []
            for i in range(len(dist_path_list)):
                data.append(self.get_csv_data(dist_path_list[i], col_name=y_label))
                data_x.append(self.get_csv_data(dist_path_list[i], col_name=x_label)) # currently assuming x has repeating columns (all columns the same)
                
        plotly_data = []
        for i in range(len(data)):
            dist_data = data[i]
            x = data_x[i]
            if isinstance(dist_data[0,0], str):
                not_null_idx = np.where(dist_data != 'None')
                row_idx = np.unique(not_null_idx[0])
                dist_data = dist_data[row_idx].astype(np.float64)
                x = x[row_idx]

            # sort
            sorted_idx = np.argsort(x[:,0])    
            x = x[sorted_idx, :]
            dist_data = dist_data[sorted_idx, :]

            if spread_interval:
                idx = np.where(x % spread_interval == 0)
                x = x[idx[0], :]
                dist_data = dist_data[idx[0],:]
                
            mean = np.mean(dist_data, axis=1)
            std = np.std(dist_data, axis=1)

            x = x[:,0] # TODO: this might cause problems
            
            if dist_data.shape[1] == 0:
                upper_traj = mean
                lower_traj = mean
                mean_traj = mean
            else:
                upper_traj = mean + std_scale * std
                lower_traj = mean - std_scale * std
                mean_traj = mean

            color_scale = (
                np.random.randint(low=0, high=254),
                np.random.randint(low=0, high=254),
                np.random.randint(low=0, high=254)
            )
            
            upper_trace = go.Scatter(
                x = x,
                y=upper_traj,
                fill=None,
                    mode='lines',
                line=dict(
                    color='rgb'+str(color_scale),
                ),
                showlegend=False,
                name=legends[i]
            )

            lower_trace = go.Scatter(
                x = x,
                y=lower_traj,
                fill="tonexty",
                mode='lines',
                line=dict(
                    color='rgb'+str(color_scale),
                ),
                showlegend=False,
                name=legends[i]
            )

            mean_trace = go.Scatter(
                #x=mean_traj,
                x = x,
                y=mean_traj,
                mode='lines',
                line=dict(
                    color='rgb('+str(color_scale)+','+str(color_scale)+','+ str(color_scale)+')',
                ),
                name=legends[i]
            )

            plotly_data += [upper_trace, lower_trace, mean_trace]

        if not title:
            title = y_label
        layout = go.Layout(
            xaxis=dict(range=[0,np.max(x)], showgrid=False, title=x_label),
            yaxis=dict(showgrid=False, title=title),
            legend=dict(x=.73, y=0.0)
        )

        fig = go.Figure(data=plotly_data, layout=layout)

        return plotly_data, layout, fig

    def plotly_distplot(self, data,  group_names, title, bin_size=0.2):
        fig = ff.create_distplot(data, group_names, bin_size)
        fig['layout'].update(title=title)
        return fig


    def plotly_bar_group_exp(self, exp_names_list, exp_dirs, filename, x_label, y_label, spread_interval=None):
        'plot bar plots for each experiment in experiment directories, average over hyperparameter data if they exists'
        y = []
        x = []
        for exp in exp_dirs:
            x_exp = []
            y_exp = []
            hyperparam_folders = glob.glob(exp + "/*/")
            for hyperparam_folder in hyperparam_folders:
                d = pd.read_csv(os.path.join(hyperparam_folder, filename))
                x_hyperparam = d[x_label].as_matrix()
                y_hyperparam = d[y_label].as_matrix()

                x_sorted_idx = np.argsort(x_hyperparam)
                x_hyperparam = x_hyperparam[x_sorted_idx]
                y_hyperparam = y_hyperparam[x_sorted_idx]

                if len(y_exp) == 0:
                    y_exp.append(y_hyperparam)
                else:
                    if y_hyperparam.size != y_exp[0].size:
                        raise ValueError(hyperparam_folder+" data length not right")
                    else:
                        y_exp.append(y_hyperparam)

            y_exp = np.array(y_exp)
            y.append(np.mean(y_exp, axis=0))
            x = x_hyperparam
            
            
        if spread_interval:
            idx = []
            for i in range(x.size):
                if x[i] % spread_interval == 0:
                    idx.append(i)
            x = x[idx]
            y_spread = []
            for yi in y:
                y_spread.append(yi[idx])
                y = y_spread

        data, layout, fig = self.plotly_bar(x, y, x_label, y_label, group_names=exp_names_list)
        return data, layout, fig
        
    def plotly_bar(self, x, y, x_label, y_label, group_names):
        '''
        y is a list of list [[A], [B], [C], ...], each element [i] is a 1D array. x is a 1D array,  group_names are the names for [A], [B], ... 
        '''

        plotly_data = []
        if len(group_names) > 0:
            for i in range(len(y)):
                trace = go.Bar(
                    x = x,
                    y = y[i],
                    name=group_names[i]
                )
                print(i)
                plotly_data.append(trace)
        else:
            trace = go.Bar(
                x = x,
                y = y[0]
            )
            plotly_data.append(trace)
                
        layout = go.Layout(
            barmode="group",
            xaxis={
                'title': x_label
            },
            yaxis={
                "title": y_label
            },
            legend=dict(x=-0.05, y=1.1)
        )

        fig = go.Figure(data=plotly_data, layout=layout)

        return plotly_data, layout, fig
        
    def get_csv_data(self, data_path, col_num=None, col_name=None):
        '''
        Obtain data matrix from csv files
        if data_path is a file, return an array with column "col_num"
        if data_path is a directory, return a matrix with column "col_num" of all csv files in that directory
        '''
        if os.path.isdir(data_path):
            csv_path_list = []
            M = np.array([])
            for root, dirs, files in os.walk(data_path):
                for name in files:
                    if "csv" in name and "~" not in name:
                        csv_path_list.append(os.path.join(root, name))
        
            for csv_path in csv_path_list:
                if col_num:
                    v = np.genfromtxt(csv_path, delimiter=',',skip_header=1, usecols={col_num})
                elif col_name:
                    df = pd.read_csv(csv_path)
                    v = df[col_name]
                else:
                    raise ValueError("Provide name or column number")
          
                if M.size == 0:
                    M = v.reshape(v.size,1)
                else:
                    row_num = min(M.shape[0], v.size)
                    M = np.c_[M[:row_num,:],v[:row_num]]
            return M

        else:
            if col_num:
                v = np.genfromtxt(data_path, delimiter=',',skip_header=1, usecols={col_num})
            elif col_name:
                df = pd.read_csv(data_path)
                v = df[col_name]
            else:
                raise ValueError("Provide name or column number")
            return v
 